A CHI APPARTIENE
Tutto il contenuto presente nel gioco è di esclusiva proprietà
del signor patrick moling appartente alla classe quarta dell'
istituto tecnico tecnologico ITT Marconi.

COPYRIGHT
Ogni canzone ed immagine è stata creata apposta così come i 
suoni sono stati generati grazie l'aiuto di un programma
quindi tutto il gioco è copyright free.

IN COSA CONSISTE
Beh, semplice, prova a raccogliere più monete possibili in 50 
secondi, stai però attento anche alle trappole che potrebbero 
ucciderti e porre fine alla tua corsa contro il tempo.

ALTRI PROGETTI IN ARRIVO
Vorrei aggiungere più trappole in modo che sia meno monotono
e randomizzarne lo spawn così come aggiungere una moneta che
vale il doppio o cose così... si potrebbe aggiungere roba
per sempre quindi prima o poi bisogna accontentarsi
