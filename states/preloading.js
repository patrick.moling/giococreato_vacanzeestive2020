export default class extends Phaser.State {
    preload() {
        //Carica tutte le immagini per i prossimi stati
        this.load.image('sky', './immagini/cielo.png')
        this.load.image('plate', './immagini/terra.png')
        this.load.image('terrenoSenzaErba', './immagini/terra2.png')
        this.load.image('coin', './immagini/gettone.png')
        this.load.image('trap', './immagini/trappola1.png')
        this.load.image('slowTrap', './immagini/trappola2.png')
        this.load.image('background', './immagini/menuImage.png')
        this.load.image('playBtn', './immagini/playButton.png')
        this.load.image('logo', './immagini/logo.png')

        this.load.spritesheet('charAnimations', './immagini/tuttiOmini.png', 76, 160, 9)

        this.load.audio('canzoneMenu', './audio/canzoneGioco.wav')
        this.load.audio('canzone','./audio/canzoneMedia.wav')   
        this.load.audio('coinCollected','./audio/coinCollected.wav')
        this.load.audio('trapOver','./audio/hit.wav')
        this.load.audio('slowerOver','./audio/slowing.wav')
    }
    create(){
        console.log("Loading completed, changing state")
        //Avvia lo stato menu
        this.state.start('Menu')
    }
}