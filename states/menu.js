
//DO NOT TOUCH THOSE
let menuMusic
let playButton

export default class extends Phaser.State {
    create() {

        //L'immagine carina del menu
        let background = this.add.image(0,0,'background')
        //E' troppo larga sull'asse y
        background.scale.setTo(1,.75)

        //L'immagine carina del logo
        let logo = this.add.image(110,505,'logo')
        //E così è più piccola e carina
        logo.scale.setTo(.5,.5)

        //Aggiungendo l'immagine del bottone gioca
        playButton = this.add.button(180,100,'playBtn',clicked)
        playButton.inputEnabled = true;

        //Se viene cliccato il bottone metti su true cambia stato
        function clicked()
        {
            //Ferma la musica
            menuMusic.stop()
            //Comincia lo stato gioco!!
            this.game.state.start('Game')
        }

        //Fai partire la musica
        menuMusic = this.add.audio('canzoneMenu',1,true)
        menuMusic.play()

    }
    update() {
        if (playButton.input.pointerOver())
        {
            playButton.alpha = .6
        }
        else
        {
            playButton.alpha = 1
        }
    }
    
}