
//DO NOT MESS WITH THOSE
let player
let slowDownSpeed = 1
let slowDownTimer
let trapSound
let timer = 0
let gameStarted = false
let string = []
let music
let coinSound
let slowTrapSound
let guideText
let inputs
let slowingTraps
let platforms
let coins
let damagingTraps
let coinText
let lifeText
let timeText
let collectedCoins = 0
let gameOver = false
let currentLife = 0
let timeRemaining = 0
let spawnTrapTime = 0

//Mess with those maybe you create something funny 
const walkSpeed = 450 //Moving speed on x axis
const jumpPower = 1000 //The power that gets at the start of the jump
const gravitySpeed = 50 //The time that needs my code to drag the plr down
const life = 100 //The starting plr life
const gameTime = 50 //The time you can play the game before it's over
const jumpSpeed = .7 //The delay between the jumps
const playerGravity = 400 //The time that needs the to drag the plr down
const maxCoinBounce = .8 //The max coin bounce could come out of random
const maxGravityCoin = 1200 //The max coin gravity could come out of random
const maxGravityTrap = 2000 //The max trap gravity could come out of random
const firstTrapDamage = 20 //The damage of the first traps
const trapsToSpawn = 2 //The traps that has to spawn at the start
const slowDownTimeEffect = 3 //The trap slows for this time
const slowDownEffect = 70 //Higher it is stronger is the slow trap
/* default settings:
walkSpeed = 450
jumpPower = 1000 
gravitySpeed = 50 
life = 100
gameTime = 50 
jumpSpeed = .7 
playerGravity = 400
maxCoinBounce = 1.2 
maxGravityCoin = 1200 
maxGravityTrap = 2000 
firstTrapDamage = 10
slowDownTimeEffect = 3
slowDownEffect = 70 
*/

export default class extends Phaser.State {

    preload() {
        //Scoprirai nella console
        string.push("A game ")
    }

    create() {

        //Dicono sia la fisica più facile da applicare, gli credo sulla parola
        this.physics.startSystem(Phaser.Physics.ARCADE)

        //Lo vedi quel bel celo dietro di te?
        let sky = this.add.sprite(0, 0, 'sky')
        
        //Aggiungi i gruppi e attiva i loro corpi in modo che abbiano la fisica
        platforms = this.add.group()
        platforms.enableBody = true
        coins = this.add.group()
        coins.enableBody = true
        damagingTraps = this.add.group()
        damagingTraps.enableBody = true
        slowingTraps = this.add.group()
        slowingTraps.enableBody = true
        string.push(" by")

        //Aggiungo il pavimento madre(sotto di te)
        uploadOnSpot(0, 550,3,.7,'plate')
        string.push(" Patrick Moling")

        //Creo delle scale con un semblice algoritmo
        let wantedLedges = 6;
        let startingY = 300;
        let ledgesPositionsX = [650,600,550,500,450,400]
        for (var i = 0; i < wantedLedges; i++)
        {
            if (i == 0)
            {
                uploadOnSpot(ledgesPositionsX[i],startingY + (48 * i),1,.5,'plate')
            }
            else
            {
                uploadOnSpot(ledgesPositionsX[i],startingY -10+ (48 * i),.17,.5,'plate')
                uploadOnSpot(ledgesPositionsX[i]+51,startingY -10+ (48 * i),(1),1,'terrenoSenzaErba')
            }
 
        }
        //Diritti d'autore :)
        for (var i = 0; i < 3; i++)
        {
            console.log(string[i])
        }

        //Aggiungi il giocatore con le sue animazioni la sua gravità, etc.
        player = this.add.sprite(100,100,'charAnimations') 
        player.animations.add('walkR',[0,1])
        player.animations.add('walkL',[2,3])
        player.animations.add('idle', [4,5,6])
        player.animations.add('jump', [7])
        player.animations.add('die', [8])
        player.animations.play('idle', 2, true);
        this.physics.arcade.enable(player)
        player.body.bounce.y = 0
        player.body.gravity.y = playerGravity
        player.body.collideWorldBounds = true;

        //Rileva gli input presi in modo globale
        inputs = this.input.keyboard.createCursorKeys()

        //Aggiungi una rilassante (ovviamente sono ironico) musichetta
        music = this.add.audio('canzone',1)

        //Aggiungi un suono di raccolta moneta
        coinSound = this.add.audio('coinCollected',.5)

        //Aggiungi un suono di tocco trappola rallentante
        slowTrapSound = this.add.audio('slowerOver',.2)

        //Aggiungi un suono di trappola contratta
        trapSound = this.add.audio('trapOver',.5)

        //Il testo in alto a sinistra del tempo è utile...
        timeText = this.add.text(10,0,'',{fontSize: '24px', fill: '#000'})

        //Il testo in alto a destra dei goldini raccolti è utile...
        coinText = this.add.text(550,0,'',{fontSize: '24px', fill: '#000'})
        coinText.text = "Coins collected: "+collectedCoins

        //Il testo in basso a destra della vita è utile...
        lifeText = this.add.text(550,500,'',{fontSize: '30px', fill: '#FFFFFF'})
        lifeText.text = "Life: "+life
        //Metti la vita corrispondente alla vita iniziale... non voglio nascere morto
        currentLife = life

        //Quel testo orribile che ti appare al centro dello schermo
        guideText = this.add.text(20,180,'',{fontSize: '54px', fill: '#000'})
        //Ho messo un paio di \n oppure non sta nemmeno sullo schermo della nasa
        guideText.text = 'Press the arrow keys to start, \ncollect the coins as you can \nbut watch out from the traps!'
        
        function uploadOnSpot(posx, posy, scalex, scaley, object)
        {
            let ledge = platforms.create(posx, posy, object)
            ledge.scale.setTo(scalex,scaley)
            ledge.body.immovable = true
        }

    }


    update() {

        /* Con questo tutto collide con tutto 
        (a me piace così anche a fine gioco) */
        this.physics.arcade.collide(player, platforms)
        this.physics.arcade.collide(coins, platforms)
        this.physics.arcade.collide(damagingTraps, platforms)
        this.physics.arcade.collide(slowingTraps, platforms)

        /* Se il gioco non è iniziato esegui solo il controllo collisione e
        tralascia il resto del codice */
        if (gameStarted == false)
        {
            //Se viene premuta freccia destra, sinistra o in alto
            if (inputs.left.isDown || inputs.up.isDown || inputs.right.isDown)
            {

                //Prossima volta non si entra quì
                gameStarted = true
                
                /* Creo delle trappole trappola in una posizione x random con 
                gravità random, trapsToSpawn è una variabile globale costante */
                for (var i = 0; i < trapsToSpawn; i++)
                {
                    //La funzione in fondo
                    spawnObject("trap",this)
                }

                spawnObject("coin",this)

                //Musica... AZIONE
                music.play()

                //Distruggi il testo tutorial che fa schifo
                guideText.kill()

                /* Il tempo rimanente è il tempo attuale sommato al tempo di gioco
                che si vuole avere ma in secondi e non millesimi quindi lo moltiplichi
                per 1000 */
                timeRemaining = this.game.time.time + (gameTime * 1000)

            }
            else
            {
                //Non andare avanti col codice
                return;
            }
        }

        /* Se il gioco è finito esegui solo il controllo collisione e
        tralascia il resto del codice */
        if (gameOver)
        {
            return;
        }

        /* Aggiorna il testo in alto a sinistra con il tempo rimanente,
        ho fatto un piccolo calcoletto per questo */
        timeText.text = 'Time remaining: '+((timeRemaining - this.game.time.time)/1000)
        
        /* Se 0 è maggiore del tempo rimanente allora è finito il tempo
        a disposizione */
        if (0 >= ((timeRemaining - this.game.time.time)) || currentLife <= 0)
        {
            //Velocità del giocatore azzerata
            player.body.velocity.x = 0
            //Gravità alta per farlo tornare a terra anche se ha saltato
            player.body.velocity.y = 800
            
            //Attivo l'animazione idle (fermo)
            player.animations.play('idle', 1, true);
            //Setto il testo in alto a sinistra con il testo game over
            timeText.text = 'GAME OVER!'
            timeText.position.setTo(80,180)
            timeText.fontSize = '100px'
            timeText.fill = '#FF0000'
            coinText.position.setTo(150,290)
            coinText.fontSize = '60px'
            lifeText.kill()
            let coinsCollected = this.add.text(195,360,'',{fontSize: '40px', fill: '#FFFF00'})
            coinsCollected.text = "You collected "+collectedCoins+" coins!"
            let gameOverFor = "outOfTime"
            if (currentLife <= 0)
            {
                gameOverFor = "outOfLife"
                player.animations.play("die")
            }
            if (gameOverFor == "outOfLife")
            {
                coinText.text = 'No lifes remaining!'
            }
            else
            {
                coinText.text = 'Out of time!'
            }
            
            /* Imposto la variabile game over su true, non si potrà più
            giocare da qui in avanti se non si refresha la pagina */
            gameOver = true
            music.stop()
            /* Esco dall'update senza eseguire il codice restante,
            il gioco è finito! */
            return;
        }

        lifeText.position.setTo(player.position.x-20, player.position.y-40)

        /* Se il giocatore entra in contatto con una moneta avvia la
        funzione getCoin e passa la moneta presa e il giocatore 
        (per ora con il giocatore non faccio niente) */
        this.physics.arcade.overlap(player, coins, getCoin, null, this)
        this.physics.arcade.overlap(player, slowingTraps, slowPlayer, null, this)
        this.physics.arcade.overlap(player, damagingTraps, damagePlayer, null, this)

        if (slowDownSpeed < 1)
        {
            if ((slowDownTimer-this.game.time.time) <= 0)
            {
                slowDownSpeed = 1
            }
        }

        //Se è premuta la freccia sinistra
        if (inputs.left.isDown) 
        {
            /* La velocità di movimento del giocatore sull'asse x è di
            -walkSpeed (variabile const globale) negativa perchè in negativo
            va a sinistra e in positivo a destra */
            player.body.velocity.x = -walkSpeed * slowDownSpeed
            //Attiva l'animazione camminata a sinistra
            player.animations.play('walkL', 4, true);
        }
        else
        {
            //Se è premuta la freccia destra
            if (inputs.right.isDown) 
            {
                /* La velocità di movimento del giocatore sull'asse x è di
                walkSpeed (variabile const globale) positiva perchè in positivo
                va a destra e in negativo a sinistra */
                player.body.velocity.x = walkSpeed * slowDownSpeed
                //Attiva l'animazione camminata a destra
                player.animations.play('walkR', 4, true);
            }
            else
            {
                /* La velocità di movimento del giocatore sull'asse x è 0,
                non vogliamo rischiare di farlo slittare dopo aver lasciato 
                un qualsiasi comando */
                player.body.velocity.x = 0;
                //Attiva l'animazione idle (fermo)
                player.animations.play('idle', 1, true);
            }
        }

        /* Un semplice controllo con un paio di variabili, se la freccia 
        in alto è premuta E is corpo del giocatore tocca terra (o suppongo
        un qualsiasi oggetto sotto di lui) E il timer (variabile costante
        che determina il tempo fra un salto e l'altro) è minore del tempo 
        attuale (andando avanti a leggere il codice capirete) */
        if (inputs.up.isDown && player.body.touching.down && timer <= this.game.time.time) 
        {
            /* La velocità sull'asse y del giocatore è -jumpPower, negativo
            perchè deve essere spinto verso l'alto */
            player.body.velocity.y = -jumpPower
            /* Il timer è uguale al tempo attuale sommato alla velocità di 
            salto moltipliacata per 1000 essendo che si parla di millisecondi */
            timer = this.game.time.time+(1000*jumpSpeed)
        }
        else
        {
            //Se il giocatore non sta toccando terra, ossia sta cadendo
            if (player.body.touching.down == false)
            {   
                /* La velocità sull'asse y del giocatore è incrementata in positivo
                perchè deve essere tirato verso il basso e per ogni "tot" che passa 
                la forza con cui viene tirato deve incrementare di gravitySpeed 
                (variabile costante globale)    */
                player.body.velocity.y += gravitySpeed
                //Attiva l'animazione jump
                player.animations.play('jump')
            }
        }
        function getCoin(player, raccolta)//Gli viene passato il giocatore e quello che ha toccato
        {
            //Distruggi quello che ha toccato
            raccolta.kill()

            //Fai partire il suono di raccolta moneta
            coinSound.play()

            spawnObject("coin",this)

            if (gameOver)
            {
                return
            }

            //Aggiungi 1 ai soldi raccolti
            collectedCoins += 1

            //Refresha il testo dei soldi con i soldi correnti
            coinText.text = "Coins collected: "+collectedCoins
        }
        function damagePlayer(player, oggetto)//Gli viene passato il giocatore e quello che ha toccato
        {
            //Distruggi quello che ha toccato
            oggetto.kill()

            //Fai partire il suono di tocco trappola
            trapSound.play()

            //La funzione in fondo
            spawnObject("trap",this)

            if (gameOver)
            {
                return
            }

            //Sottrai il danno della prima trappola alla vita del giocatore
            currentLife -= firstTrapDamage

            //Refresha il testo dei soldi con la vita rimanente
            lifeText.text = "Life: "+currentLife
        }
        function spawnObject(object, game)
        {
            if (object == "trap")
            {
                if (Math.random()>.5)
                {
                    //Creane uno nuovo, sempre posizione e gravità random
                    let trap = damagingTraps.create(Math.random()*711, 0, 'trap')
                    trap.scale.setTo(.5,.6)
                    game.physics.arcade.enable(trap)
                    trap.body.bounce.y = 0
                    trap.body.gravity.y = ((Math.random()*maxGravityTrap)+400)
                }
                else
                {
                    //Creane uno nuovo, sempre posizione e gravità random
                    let trap = slowingTraps.create(Math.random()*760, 0, 'slowTrap')
                    trap.scale.setTo(.5,.6)
                    game.physics.arcade.enable(trap)
                    trap.body.bounce.y = 0
                    trap.body.gravity.y = ((Math.random()*maxGravityTrap)+400)
                }
            }
            else if (object == "coin")
            {
                //Creane uno nuovo, sempre posizione e gravità random
                let moneta = coins.create(Math.random()*760, 0, 'coin')
                game.physics.arcade.enable(moneta)
                moneta.body.bounce.y = (Math.random()*maxCoinBounce)
                moneta.body.gravity.y = ((Math.random()*maxGravityCoin)+400)
            }
        }
        function slowPlayer(player, oggetto)//Gli viene passato il giocatore e quello che ha toccato
        {
            //Distruggi quello che ha toccato
            oggetto.kill()

            //Fai partire il suono di tocco trappola rallentante
            slowTrapSound.play()

            //La funzione in fondo
            spawnObject("trap",this.game)

            //Rallenta il giocatore
            slowDownSpeed -= (Math.random()*slowDownEffect/100)

            if (slowDownSpeed < 0)
            {
                slowDownSpeed = 0
            }
            //Timer di rallentamento
            slowDownTimer = this.game.time.time + slowDownTimeEffect * 1000
        }
    }
}


