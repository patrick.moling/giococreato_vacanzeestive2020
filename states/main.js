//Importa tutti i file per impostare gli stati, questo file deve essere di tipo MODULO
import preloading from './preloading.js'
import menu from './menu.js'
import game from './game.js'

class Game extends Phaser.Game {
    constructor() {

        //Configurando il gioco
        super(800,600, Phaser.AUTO)
        Phaser.ScaleManager.scaleMode = Phaser.ScaleManager.SHOW_ALL
        Phaser.ScaleManager.pageAlignHorizontally = true
        Phaser.ScaleManager.pageAlignVertically = true
        //Phaser.Game.backgroundColor = '#eee'

        //Aggiungendo gli stati
        this.state.add('Preload', preloading)
        this.state.add('Menu', menu)
        this.state.add('Game', game)

        console.log("Starting state system")

        //Cominciamo con lo stato preload
        this.state.start('Preload')
    }
}

new Game()